''' Move for Lola on behalf of REMAP 
	Prototype
	This project has a Git repository here:
	https://gitlab.com/Remap_Charity/move

    Edit it from VSC using rmate on the pi:
    To open the SSH line for rmate to use:
     PC: ssh -R 52698:localhost:52698 pi@192.168.1.102
    Then on Pi type this:
     Pi: rmate move.py
    
'''
# import the necessary packages
#from picamera.array import PiRGBArray
from picamera import PiCamera
from imutils.video.pivideostream import PiVideoStream
import imutils
import time
import cv2
import numpy as np
import pygame
from subprocess import call
#import RPi.GPIO as GPIO  

__version__ = "v0.1"

FULL_SCREEN = True

borderWidth = 50	#Default to 50 but can be tweaked
imageWidth = 640	#512
imageHeight = 480	#608

# Use the Broadcom SOC Pin numbers 
# Setup the Pin with Internal pullups enabled and PIN in reading mode. 
#GPIO.setmode(GPIO.BCM)  
#GPIO.setup(18, GPIO.IN, pull_up_down = GPIO.PUD_UP)  

# Our function on what to do when the button is pressed 
def Shutdown(channel):  
   os.system("sudo shutdown -h now")  

#GPIO.add_event_detect(18, GPIO.FALLING, callback = Shutdown, bouncetime = 2000)  

def strokeWatchdog():
	''' This starts the watchdog going and keeps it going '''
	f = open("/dev/watchdog", "w")
	f.write("S")
	f.close
	
def stopWatchdog():
	''' this stops the watchdog rebooting the system '''
	f = open("/dev/watchdog", "w")
	f.write("V")
	f.close
	print("Watchdog stopped")

windowName = "Move"
font = cv2.FONT_HERSHEY_SIMPLEX
batteryWarning = False
text = "Move"
firstFrame = None

# created a *threaded *video stream, allow the camera sensor to warmup,
vs = PiVideoStream(resolution=(imageWidth, imageHeight), framerate=15).start()
time.sleep(1.0)

# capture frames from the camera
#for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True)
while True:
	''' Do this forever'''
	text = "-"
	frame = vs.read()
	#strokeWatchdog()
	cv2.namedWindow(windowName, cv2.WINDOW_NORMAL)
	# Make the window match the LCD resolution
	cv2.resizeWindow(windowName, 1024, 600)
	# Make the window full-Screen
	if FULL_SCREEN: cv2.setWindowProperty(windowName, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
	
	# resize the frame, convert it to grayscale, and blur it
	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)
 
	# if the first frame is None, initialize it
	if firstFrame is None:
		firstFrame = gray
		continue
	
	# compute the absolute difference between the current frame and
	# first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
 
	# dilate the thresholded image to fill in holes, then find contours
	# on thresholded image
	thresh = cv2.dilate(thresh, None, iterations=2)
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
 
	# loop over the contours
	min_area = 1000
	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < min_area:
			continue
 
		# compute the bounding box for the contour, draw it on the frame,
		# and update the text
		(x, y, w, h) = cv2.boundingRect(c)
		cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
		text = "Motion"

	# Display the image
	cv2.imshow(windowName, frame)

	# Get keyboard input
	key = cv2.waitKey(1) & 0xFF
 
	# clear the stream in preparation for the next frame
	#rawCapture.truncate(0)
 
	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		# Stop the capture thread
		vs.stop()
		break
	elif key == ord("c"):
		firstFrame = None
	elif key == ord("s"):
		stopWatchdog()

